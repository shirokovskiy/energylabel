<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 5/20/15
 * Time         : 1:21 PM
 * Description  :
 */

class Groupxs_EnergyLabel_Model_Observer extends Varien_Event_Observer {

    protected $energyClass = array('A++','A+','A','B','C','D','E');
    protected $energyClassColors = array('A++'=>'009f3c', 'A+'=>'22b431', 'A'=>'b3d807', 'B'=>'f8f400', 'C'=>'f4c100', 'D'=>'e97017', 'E'=>'df0024');

    /**
     * Create Energy Label for product
     *
     * @param Varien_Event_Observer $observer
     * @param bool $regenerate
     */
    public function generateLabelImage(Varien_Event_Observer $observer, $regenerate = false) {
        if (!Mage::helper('energylabel')->getIsEnabled()){ //if module is not enabled
            if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Module is not Enabled! '.__METHOD__.' as line:'.__LINE__);
            return $this;
        }

        if (Mage::registry('generate_image_event_dispatched') == 1){ //do nothing if the event was dispatched already
            if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Don`t run it twice! '.__METHOD__.' as line:'.__LINE__);
            return $this;
        }

        /**
         * @var Groupxs_EnergyLabel_Model_Product $product
         */
        $product = $observer->getEvent()->getProduct();

        /**
         * If backend event
         */
        if ($observer->getEvent()->getName() != 'catalog_controller_product_view') {
            if (!$this->_isProductChanged($product)) {
                return $this;
            }
        }


        /**
         * If Admin Panel event happens, then let's recreate/regenerate image,
         * even if it exists.
         */
        if ($product->getXsEnergyLabel() != false && $product->getXsEnergyLabel() !== 'no_selection' && !$regenerate) {
            if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('IMAGE EXIST: '.$product->getXsEnergyLabel());
        } else {
            # if there is no image, or we want to regenerate it, let's create it and upload according parameters
            $energyClass = strtoupper($product->getXsEfficiencyClass());
            $energyConsumption = $product->getXsEnergyConsumption();
            $productType = (string) $product->getResource()->getAttribute('xs_product_type')->getFrontend()->getValue($product);
            $labelCompany = $product->getXsCompanyName();
            $labelEan = $product->getXsEan();

            if (empty($energyClass)) {
                if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Warning: no $energyClass value');
            }
            if (empty($energyConsumption)) {
                if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Warning: no $energyConsumption value');
            }
            if (empty($productType) || $productType == 'No') {
                if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Warning: no $productType value');
            }
            if (empty($labelCompany)) {
                if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Warning: no $labelCompany value');
            }
            if (empty($labelEan)) {
                if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Warning: no $labelEan value');
            }

            if (empty($energyClass)
                && empty($energyConsumption)
                && (empty($productType) || $productType == 'No')
                && empty($labelCompany)
                && empty($labelEan)
            ) {
                // Let's check if this Product is Configurable
                if ($product->getTypeId() == "configurable") {
                    $used_products = $product->getTypeInstance(true)->getUsedProducts(null, $product);
                    if (is_array($used_products) && !empty($used_products)) {
                        $isData = false;
                        foreach ($used_products as $k => $simple) {
                            $simple = $simple->load($simple->getId());

                            $energyClass = strtoupper($simple->getXsEfficiencyClass());
                            $energyConsumption = $simple->getXsEnergyConsumption();
                            $productType = (string) $simple->getResource()->getAttribute('xs_product_type')->getFrontend()->getValue($simple);
                            $labelCompany = $simple->getXsCompanyName();
                            $labelEan = $simple->getXsEan();

                            if (empty($energyClass)
                                && empty($energyConsumption)
                                && (empty($productType) || $productType == 'No')
                                && empty($labelCompany)
                                && empty($labelEan)
                            ) {
                                continue;
                            } else {
                                $isData = true;
                                break;
                            }
                        }

                        if (!$isData) {
                            if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Warning: no reason to create EL for CP');
                            return $this;
                        }
                    }
                } else {
                    if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Warning: no reason to create EL.');
                    return $this;
                }
            }

            if (in_array($productType, array('Lamp', 'LED Lamp', 'Light Bulb')) && !empty($labelCompany) && !empty($labelEan)) {
                if ( $productType == 'Light Bulb' ) {
                    $energyLevel = array_search($energyClass, $this->energyClass);

                    if ($energyLevel === false) {
                        Mage::throwException(Mage::helper('catalog/product')->__('Unknown Energy Label Efficiency class! Use one of: ') . implode(',', $this->energyClass));
                    }
                }

                /**
                 * Let's work with images / fonts / paths.
                 *
                 * File settings
                 */
                $imageEnergyLabelBulb = 'energyLabel-Bulb.png';
                $imageEnergyLabelLamp = 'energyLabelLamp.png';
                $imageEnergyLabelLampLed = 'energyLabelLamp-Led.png';
                if (!empty($energyClass))
                    $imagePointer = 'pointer'.DS.$energyClass.'.png';
                $fontArial = 'font-arial.ttf';
                $fontCalibri = 'font-calibri.ttf';
                $fontCalibriBold = 'font-calibri-bold.ttf';

                /**
                 * Path settings
                 */
                $imagePath = Mage::getBaseDir("media").DS.'groupxs'.DS;
                $imageTmpPath = Mage::getBaseDir("media").DS.'tmp'.DS;
                $imageLabelBulb = $imagePath . $imageEnergyLabelBulb;
                $imageLabelLamp = $imagePath . $imageEnergyLabelLamp;
                $imageLabelLampLed = $imagePath . $imageEnergyLabelLampLed;
                if (!empty($imagePointer))
                    $imagePointer = $imagePath . $imagePointer;
                $fontArial = $imagePath. $fontArial;
                $fontCalibri = $imagePath. $fontCalibri;
                $fontCalibriBold = $imagePath. $fontCalibriBold;

                if (!is_dir($imageTmpPath)) {
                    @mkdir($imageTmpPath, 0775, true);
                }

                /**
                 * Let`s draw and save small label near price
                 * i.e. $energyClass = 'C'
                 */
                if (!empty($energyClass) && array_key_exists($energyClass, $this->energyClassColors)) {
                    $imageSmallLabelPrice = $energyClass.'.png';
                    $imageSmallLabelPrice = $imagePath . "price-labels". DS .$imageSmallLabelPrice;

                    if (file_exists($imageSmallLabelPrice)) {
                        $fileName = 'energySmallLabel.'. $product->getId().'.png';

                        if (false === copy($imageSmallLabelPrice, $imageTmpPath . $fileName)) {
                            if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Cannot copy file '.$imageSmallLabelPrice.' into '.$imageTmpPath . $fileName);
                        }
                    }
                } else {
                    if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Can`t create small Label near Price');
                }

                switch ($productType) {
                    case 'Light Bulb':
                        if (!empty($imageLabelBulb) && file_exists($imageLabelBulb)) {
                            // Get start generation
                            $imageLabelBulb = imagecreatefrompng($imageLabelBulb);
                            $imageLabelW = imagesx($imageLabelBulb);
                            $imageLabelH = imagesy($imageLabelBulb);

                            // Create Canvas according main template
                            $canvasLabel = imagecreatetruecolor($imageLabelW,$imageLabelH);
                            imagesavealpha($canvasLabel, true);
                            $trans_color = imagecolorallocatealpha($canvasLabel, 0, 0, 0, 127);
                            imagefill($canvasLabel, 0, 0, $trans_color);

                            // Merge main template with canvas
                            imagecopy($canvasLabel /* canvas */
                                , $imageLabelBulb /* image */
                                , 0 /* Position X of canvas where we put image */
                                , 0 /* Position Y of canvas where we put image */
                                , 0 /* X offset of image. In this task always = 0, because we won't cut off image */
                                , 0 /* Y offset of image. In this task always = 0, because we won't cut off image */
                                , $imageLabelW /* Width of image above */
                                , $imageLabelH /* Height of image above */);

                            // Set colors
                            $black = imagecolorallocate($canvasLabel, 0, 0, 0);

                            // Prepare Energy Class pointer
                            if (file_exists($imagePointer)) {
                                $imagePointer = imagecreatefrompng($imagePointer);
                                $energyMarkStep = imagesy($imagePointer) - 14.5; /* pixels */
                                $energyMarkBoxStartX = $imageLabelW * 0.6; /* %% */
                                $energyMarkBoxStartY = $imageLabelH * 0.28; /* %% */

                                $energyMarkPositionX = $energyMarkBoxStartX;
                                $energyMarkPositionY = $energyMarkBoxStartY + ($energyLevel * $energyMarkStep);

                                // Merge pointer template above canvas with template
                                imagecopy($canvasLabel
                                    , $imagePointer
                                    , $energyMarkPositionX
                                    , $energyMarkPositionY
                                    , 0
                                    , 0
                                    , imagesx($imagePointer)
                                    , imagesy($imagePointer));
                            } else {
                                if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Pointer of Energy Label not found.');
                            }

                            // Draw text 'kWh/1000h'
                            if (!empty($energyConsumption)) {
                                $iDec = (floatval($energyConsumption) - intval($energyConsumption) > 0 ? 2 : 0);
                                $energyConsumption = number_format($energyConsumption,$iDec);
                                $fontSize = 47;
                                $font = $fontCalibriBold;
                                // get text length
                                $dimensions = imagettfbbox($fontSize, 0, $font, $energyConsumption);
                                $textWidth = abs($dimensions[4] - $dimensions[0]);

                                imagettftext($canvasLabel, $fontSize, /* angle */ 0, /* X */ intval($imageLabelW * 0.47) - $textWidth, /* Y */ intval($imageLabelH * 0.95), $black, $font, $energyConsumption);
                            }

                            // Draw text - Company name
                            if (!empty($labelCompany)) {
                                imagettftext($canvasLabel, /* Font Size */ 24, /* angle */ 0, /* X */ intval($imageLabelW * 0.08), /* Y */ intval($imageLabelH * 0.24 /* % from top */), $black, $fontArial, $labelCompany);
                            }

                            // Draw text - EAN
                            if (!empty($labelEan)) {
                                $fontSize = 24;
                                $font = $fontArial;
                                // get text length
                                $dimensions = imagettfbbox($fontSize, 0, $font, $labelEan);
                                $textWidth = abs($dimensions[4] - $dimensions[0]);
                                $startXtext = $imageLabelW - $textWidth;

                                imagettftext($canvasLabel, $fontSize, /* angle */ 0, /* X */ $startXtext - intval($imageLabelW * 0.09), /* Y */ intval($imageLabelH * 0.24 /* % from top */), $black, $font, $labelEan);
                                unset($fontSize, $font);
                            }

                            // Save image
                            $fileName = 'energyLabel.'. $product->getId().'.png';
                            imagepng($canvasLabel, $imageTmpPath . $fileName);

                            // Clean images
                            imagedestroy($imageLabelBulb);
                            imagedestroy($imagePointer);
                            imagedestroy($canvasLabel);
                        } else {
                            if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Can`t create Energy Label for '.$productType);
                        }
                        break;
                    case 'Lamp':
                        $imageLampLabel = $imageLabelLamp;
//                        $labelDescription = 'Diese Leuchte ist geeignet für Leuchtmittel der Energieklassen:';
//                        $supportedClasses = 'A++,A+,A,B,C,D,E';
                        break;
                    case 'LED Lamp':
                        $imageLampLabel = $imageLabelLampLed;
//                        $labelDescription = 'Diese Leuchte enthält eingebaute LED-Lampen';
//                        $labelBottomDescription = "Die Lampen können in der Leuchte nicht ausgetauscht werden.";
//                        $supportedClasses = 'A++,A+,A';
                        break;
                    default:
                        break;
                }

                /**
                 * Let's check necessity to create Label of Lamp
                 */
                if (!empty($imageLampLabel) && file_exists($imageLampLabel)) {
                    // Get start generation
                    $imageLampLabel = imagecreatefrompng($imageLampLabel);
                    $imageLampLabelW = imagesx($imageLampLabel);
                    $imageLampLabelH = imagesy($imageLampLabel);

                    // Create Canvas according main template
                    $canvasLampLabel = imagecreatetruecolor($imageLampLabelW,$imageLampLabelH);
                    imagesavealpha($canvasLampLabel, true);
                    $transparentColor = imagecolorallocatealpha($canvasLampLabel, 0, 0, 0, 127);
                    imagefill($canvasLampLabel, 0, 0, $transparentColor);

                    // Set color to draw text
                    $black = imagecolorallocate($canvasLampLabel, 0, 0, 0);

                    // Merge images
                    imagecopy($canvasLampLabel /* canvas */
                        , $imageLampLabel /* image */
                        , 0 /* Position X of canvas where we put image */
                        , 0 /* Position Y of canvas where we put image */
                        , 0 /* X offset of image. In this task always = 0, because we won't cut off image */
                        , 0 /* Y offset of image. In this task always = 0, because we won't cut off image */
                        , $imageLampLabelW /* Width of image above */
                        , $imageLampLabelH /* Height of image above */);

                    $fontSize = 26;

                    // Draw text - Company name
                    if (!empty($labelCompany)) {
                        imagettftext($canvasLampLabel, $fontSize, /* angle */ 0,
                            intval($imageLampLabelW * 0.07) /* XX% from left */,
                            intval($imageLampLabelH * 0.14) /* YY% from top */, $black, $fontArial, $labelCompany);
                    }

                    // Draw text - EAN
                    if (!empty($labelEan)) {
                        $font = $fontArial;
                        // get text length
                        $dimensions = imagettfbbox($fontSize, 0, $font, $labelEan);
                        $textWidth = abs($dimensions[4] - $dimensions[0]);
                        $startXtext = $imageLampLabelW - $textWidth;

                        imagettftext($canvasLampLabel, $fontSize, /* angle */ 0,
                            $startXtext - intval($imageLampLabelW * 0.08) /* XX% from left */,
                            intval($imageLampLabelH * 0.14) /* YY% from top */, $black, $font, $labelEan);
                    }

                    // Draw text - Description
                    if (!empty($labelDescription)) {
                        /**
                         * Let's set line breaks of long description
                         */
                        $fontSize = 28;
                        $font = $fontCalibri;
                        $maxWidth = intval($imageLampLabelW * 0.6); // pixels
                        $words = explode(" ",$labelDescription);
                        $wNum = count($words);
                        $fontSize = $wNum > 13 ? $fontSize - 6 : $fontSize;
                        $fontSize = $wNum > 23 ? $fontSize - 8 : $fontSize;
                        $line = $text = '';
                        for($i=0; $i<$wNum; $i++){
                            $line .= $words[$i];
                            $dimensions = imagettfbbox($fontSize, 0, $font, $line);
                            $lineWidth = $dimensions[2] - $dimensions[0];
                            if ($lineWidth > $maxWidth) {
                                $text.=(!empty($text) ? PHP_EOL.$words[$i].' ' : $words[$i].' ');
                                $line = $words[$i].' ';
                            }
                            else {
                                $text.=$words[$i].' ';
                                $line.=' ';
                            }
                        }
                        // $text .= ' '.$fontSize.' '.$wNum;
                        imagettftext($canvasLampLabel, $fontSize, /* angle */ 0,
                            intval($imageLampLabelW * 0.35 /* XX% from left */),
                            intval($imageLampLabelH * 0.19 /* YY% from top */), $black, $font, $text);
                    }

                    /**
                     * Draw text for bottom part
                     * Let's set line breaks of long description
                     */
                    if (!empty($labelBottomDescription)) {
                        $fontSize = 23;
                        $font = $fontCalibriBold;
                        $maxWidth = intval($imageLampLabelW * 0.8); // pixels
                        $words = explode(" ", $labelBottomDescription);
                        $wNum = count($words);
                        $line = $text = '';
                        for ($i = 0; $i < $wNum; $i++) {
                            $line .= $words[$i];
                            $dimensions = imagettfbbox($fontSize, 0, $font, $line);
                            $lineWidth = $dimensions[2] - $dimensions[0];
                            if ($lineWidth > $maxWidth) {
                                $text .= (!empty($text) ? PHP_EOL . $words[$i] . ' ' : $words[$i] . ' ');
                                $line = $words[$i] . ' ';
                            } else {
                                $text .= $words[$i] . ' ';
                                $line .= ' ';
                            }
                        }
                        // $text .= ' '.$fontSize.' '.$wNum;
                        imagettftext($canvasLampLabel, $fontSize, 0 /* angle */,
                            intval($imageLampLabelW * 0.07 /* XX% from left */),
                            intval($imageLampLabelH * 0.8 /* YY% from top */), $black, $font, $text);
                    }

                    // Save image for second view of Label
                    $fileName = 'energyLabel.'. $product->getId().'.png';
                    imagepng($canvasLampLabel, $imageTmpPath . $fileName);

                    // Clean images
                    imagedestroy($imageLampLabel);
                    imagedestroy($canvasLampLabel);
                }

                /**
                 * Request image saving process if not in backend
                 */
                if ($observer->getEvent()->getName() == 'catalog_controller_product_view') {
                    $this->saveLabelImage($observer);
                }
            } else {
                if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Wrong EAN, Company or Product type');
            }
        }

        if (is_null(Mage::registry('generate_image_event_dispatched'))){
            Mage::register('generate_image_event_dispatched', 1); //mark the event as dispatched.
        }

        return $this;
    }

    private function _isProductChanged(&$product) {
        $product_old = Mage::getModel('catalog/product')->load($product->getId());

        if ($product->getData('xs_efficiency_class') == $product_old->getData('xs_efficiency_class')
            && $product->getData('xs_energy_consumption') == $product_old->getData('xs_energy_consumption')
            && $product->getData('xs_company_name') == $product_old->getData('xs_company_name')
            && $product->getData('xs_ean') == $product_old->getData('xs_ean')
            && $product->getResource()->getAttribute('xs_product_type')->getFrontend()->getValue($product)
            == $product_old->getResource()->getAttribute('xs_product_type')->getFrontend()->getValue($product_old)
        ) {
            // nothing to change OR image has been removed manually from back-end
            if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Nothing to change OR image was removed manually from back-end! '.__METHOD__.' at line:'.__LINE__);
            return false;
        }
        return true;
    }

    /**
     * This separate method is necessary for event 'catalog_product_save_after' because we can't save product images
     * inside event 'catalog_product_save_before'
     *
     * @param $observer
     * @return $this
     */
    public function saveLabelImage($observer) {
        if (Mage::registry('save_label_image_event_was_dispatched') == 1){ //do nothing if the event was dispatched
            if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Don`t run it twice! '.__METHOD__);
        } else {
            /**
             * @var Groupxs_EnergyLabel_Model_Product $product
             */
            $product = $observer->getEvent()->getProduct();

            if (!$product instanceof Groupxs_EnergyLabel_Model_Product) {
                $productID = $product->getId();
                $product = new Groupxs_EnergyLabel_Model_Product();
                $product->load($productID);
            }

            $imageTmpPath = Mage::getBaseDir("media").DS.'tmp'.DS;
            $preFix = rand(10,99);
            $fileName = 'energyLabel.'. $product->getId().'.png';
            $fileNameFinal = $preFix.'energyLabel.'. $product->getId().'.png';
            $fileNameS = 'energySmallLabel.'. $product->getId().'.png';
            $fileNameFinalS = $preFix.'energySmallLabel.'. $product->getId().'.png';
            $imgPreGeneratedPath = $imageTmpPath . $fileName;
            $imgPreGeneratedPathS = $imageTmpPath . $fileNameS;
            $imgGeneratedPath = $imageTmpPath . $fileNameFinal;
            $imgGeneratedPathS = $imageTmpPath . $fileNameFinalS;
            $dirMageMediaGalleryPath = Mage::getBaseDir('media').DS.'catalog'.DS.'product'; // without final DS (!!!)
            $isHasToBeSaved = false;

            if (file_exists($imgPreGeneratedPath) || file_exists($imgPreGeneratedPathS)) {
                // To work with images we need full data for media gallery
                $product = $product->load($product->getId());
                $isHasToBeSaved = true;
            }

            /**
             * Just to be sure...
             */
            if (file_exists($imgPreGeneratedPath)) {
                try {
                    rename($imgPreGeneratedPath, $imgGeneratedPath);
                } catch (Exception $ex) {
                    Mage::logException($ex);
                }

                if ($product->getXsEnergyLabel() && $product->getXsEnergyLabel() !== 'no_selection') {
                    // Current energy label filename
                    $mediaPathFileName = $product->getXsEnergyLabel();

                    // Let's remove old Label from Product
                    if (file_exists($dirMageMediaGalleryPath.$mediaPathFileName)) {
                        $product->removeLabelFromMediaGallery($mediaPathFileName);
                        unlink($dirMageMediaGalleryPath.$mediaPathFileName); // we have to remove it, otherwise Magento will rename the same filenames adding postfix
                    }
                }

                // Let's upload Label to Product
                if (file_exists($imgGeneratedPath)) {
                    $product->addImageToMediaGallery($imgGeneratedPath, array('xs_energy_label'), /*move*/ !isset($_SERVER['MAGE_IS_DEVELOPER_MODE']), /*exclude*/ false);
                } else {
                    if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Nothing has been add to media gallery.');
                }
            }

            if (file_exists($imgPreGeneratedPathS)) {
                try {
                    rename($imgPreGeneratedPathS, $imgGeneratedPathS);
                } catch (Exception $ex) {
                    Mage::logException($ex);
                }

                if ($product->getXsEnergyLabelMark() && $product->getXsEnergyLabelMark() !== 'no_selection') {
                    // Current energy label mark's filename
                    $mediaPathFileName = $product->getXsEnergyLabelMark();

                    // Let's remove old Label from Product
                    if (file_exists($dirMageMediaGalleryPath.$mediaPathFileName)) {
                        $product->removeLabelFromMediaGallery($mediaPathFileName);
                        unlink($dirMageMediaGalleryPath.$mediaPathFileName); // we have to remove it, otherwise Magento will rename the same filenames adding postfix
                    }
                }

                // Let's upload Label to Product
                if (file_exists($imgGeneratedPathS)) {
                    $product->addImageToMediaGallery($imgGeneratedPathS, array('xs_energy_label_mark'), /*move*/ !isset($_SERVER['MAGE_IS_DEVELOPER_MODE']), /*exclude*/ false);
                } else {
                    if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) Mage::log('Nothing has been add to media gallery as mark.');
                }
            }

            if ($isHasToBeSaved) {
                // Do not remove it before $product->save(); Otherwise loop forever
                if (is_null(Mage::registry('save_label_image_event_was_dispatched'))){
                    Mage::register('save_label_image_event_was_dispatched', 1); //mark the event as dispatched.
                }

                if ($observer->getEvent()->getName() == 'catalog_controller_product_view') {
                    # Emulate CMS environment if Front-end
                    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                }

                $product->save();
            }
        }

        if (is_null(Mage::registry('save_label_image_event_was_dispatched'))){
            Mage::register('save_label_image_event_was_dispatched', 1); //mark the event as dispatched.
        }

        return $this;
    }

    public function hex2RGB($hexStr, $returnAsString = false, $seperator = ',') {
        $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
        $rgbArray = array();
        if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
            $colorVal = hexdec($hexStr);
            $rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
            $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
            $rgbArray['blue'] = 0xFF & $colorVal;
        } elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
            $rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
            $rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
            $rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
        } else {
            return false; //Invalid hex color code
        }
        return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array
    }

    public function regenerateLabelImage(Varien_Event_Observer $observer) {
        return $this->generateLabelImage($observer, true);
    }
}
