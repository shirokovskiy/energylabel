<?php
class Groupxs_EnergyLabel_Model_Product extends Mage_Catalog_Model_Product
{
    public function removeLabelFromMediaGallery($file)
    {
        $attributes = $this->getTypeInstance(true)->getSetAttributes($this);
        if (!isset($attributes['media_gallery'])) {
            return $this;
        }
        $mediaGalleryAttribute = $attributes['media_gallery'];
        /* @var $mediaGalleryAttribute Mage_Catalog_Model_Resource_Eav_Attribute */
        $mediaGalleryAttribute->getBackend()->removeImage($this, $file);

        return $this;
    }
}
