<?php
/**
 * Created by   : Dmitry Shirokovskiy.
 * Email        : info@phpwebstudio.com
 * Date         : 5/19/15
 * Time         : 2:27 PM
 * Description  : New product's attribute
 *
 *
 */
$this->startSetup();
$this->addAttribute('catalog_product'                   // could be 'catalog_category'
    , 'xs_company_name' 							    // Set attribute's Code
    , array(
        'group'			    => 'Energy Label'           // Set attribute's Group name
        , 'input'			=> 'text'                   // more: 'select', 'boolean' (same as 'select' but only 'Yes/No'), 'textarea', 'image', 'media_image'
        , 'type'			=> 'varchar'                // more: 'datetime', 'decimal', 'int', 'text', 'varchar'
        , 'label'			=> 'Company'	            // Set attribute's Label
        , 'backend'			=> ''                       // backend_model
        , 'frontend'        => ''                       // fronted_model
        , 'frontend_class'  => 'validate-length maximum-length-9' //
        , 'class'           => ''                       // CSS class: required-entry, validate-number, etc. (see js/prototype/validation.js)
        , 'default'         => ''                       //
        , 'table'           => ''
        , 'note'            => ''
        , 'apply_to'        => 'simple'
        , 'position'		=> 10
        , 'visible'			=> 0
        , 'visible_on_front'=> 0
        , 'visible_in_advanced_search'  => 0
        , 'filterable'		=> 0
        , 'filterable_in_search'        => 0
        , 'searchable'		=> 0
        , 'comparable'		=> 0
        , 'unique'		    => 0                        // or false
        , 'required'		=> 0                        // or false
        , 'user_defined'	=> 1                        // or true
        , 'wysiwyg_enabled' => 0
        , 'is_configurable' => 0
        , 'used_for_sort_by'=> 0
        , 'used_for_promo_rules'        => 0
        , 'used_in_product_listing'     => 0
        , 'is_html_allowed_on_front'    => 0
        , 'is_used_for_price_rules'     => 0
        , 'attribute_model' => NULL
        , 'input_renderer'  => NULL
        , 'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
    )
);

$this->addAttribute('catalog_product'
    , 'xs_efficiency_class'
    , array(
        'group'			    => 'Energy Label'
        , 'input'			=> 'text'
        , 'type'			=> 'varchar'
        , 'label'			=> 'Efficiency class'
        , 'backend'			=> ''
        , 'frontend'        => ''
        , 'frontend_class'  => 'validate-length maximum-length-3'
        , 'class'           => ''
        , 'default'         => ''
        , 'visible'			=> 1
        , 'filterable'		=> 0
        , 'searchable'		=> 0
        , 'comparable'		=> 0
        , 'visible_on_front'=> 0
        , 'unique'		    => 0
        , 'required'		=> 0
        , 'user_defined'	=> 1
        , 'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
    )
);

$this->addAttribute('catalog_product'
    , 'xs_ean'
    , array(
        'group'			    => 'Energy Label'
        , 'input'			=> 'text'
        , 'type'			=> 'varchar'
        , 'label'			=> 'EAN Code Number'
        , 'backend'			=> ''
        , 'frontend'        => ''
        , 'frontend_class'  => 'validate-digits validate-length maximum-length-16'
        , 'class'           => ''
        , 'default'         => ''
        , 'visible'			=> 1
        , 'filterable'		=> 0
        , 'searchable'		=> 0
        , 'comparable'		=> 0
        , 'visible_on_front'=> 0
        , 'unique'		    => 0
        , 'required'		=> 0
        , 'user_defined'	=> 1
        , 'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
    )
);

$this->addAttribute('catalog_product'
    , 'xs_energy_consumption'
    , array(
        'group'			    => 'Energy Label'
        , 'input'			=> 'text'
        , 'type'			=> 'decimal'
        , 'label'			=> 'Energy consumption, kWh per 1000 hours'
        , 'backend'			=> ''
        , 'frontend'        => ''
        , 'frontend_class'  => 'validate-number validate-length maximum-length-9'
        , 'class'           => ''
        , 'default'         => ''
        , 'visible'			=> 1
        , 'filterable'		=> 0
        , 'searchable'		=> 0
        , 'comparable'		=> 0
        , 'visible_on_front'=> 0
        , 'unique'		    => 0
        , 'required'		=> 0
        , 'user_defined'	=> 1
        , 'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
    )
);

$this->addAttribute('catalog_product'
    , 'xs_product_type'
    , array(
        'group'			    => 'Energy Label'
        , 'input'			=> 'select'
        , 'type'			=> 'int'
        , 'label'			=> 'Type of product'
        , 'backend'			=> 'eav/entity_attribute_backend_array'
        , 'source'			=> 'eav/entity_attribute_source_table'
        , 'frontend'        => ''
        , 'class'           => ''
        , 'default'         => ''
        , 'visible'			=> 1
        , 'filterable'		=> 0
        , 'searchable'		=> 0
        , 'comparable'		=> 0
        , 'visible_on_front'=> 0
        , 'unique'		    => 0
        , 'required'		=> 0
        , 'user_defined'	=> 1
        , 'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
        , 'option'          => array (
            'values'        => array (
                1 => 'Lamp'             // DO NOT FIX IT!!! OR look at Observer.
                , 2 => 'Light Bulb'     // DO NOT FIX IT!!! OR look at Observer.
            )
        )
    )
);

$this->addAttribute('catalog_product'
    , 'xs_supported_classes'
    , array(
        'group'			    => 'Energy Label'
        , 'input'			=> 'text'
        , 'type'			=> 'varchar'
        , 'label'			=> 'Lamp supported energy classes'
        , 'backend'			=> ''
        , 'frontend'        => ''
        , 'frontend_class'  => 'validate-length maximum-length-16'
        , 'class'           => ''
        , 'default'         => ''
        , 'visible'			=> 1
        , 'filterable'		=> 0
        , 'searchable'		=> 0
        , 'comparable'		=> 0
        , 'visible_on_front'=> 0
        , 'unique'		    => 0
        , 'required'		=> 0
        , 'user_defined'	=> 1
        , 'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
    )
);

$this->addAttribute('catalog_product'
    , 'xs_efficiency_combination'
    , array(
        'group'			    => 'Energy Label'
        , 'input'			=> 'text'
        , 'type'			=> 'varchar'
        , 'label'			=> 'Efficiency class of lamp and light bulb combination'
        , 'backend'			=> ''
        , 'frontend'        => ''
        , 'frontend_class'  => 'validate-length maximum-length-3'
        , 'class'           => ''
        , 'default'         => ''
        , 'visible'			=> 1
        , 'filterable'		=> 0
        , 'searchable'		=> 0
        , 'comparable'		=> 0
        , 'visible_on_front'=> 0
        , 'unique'		    => 0
        , 'required'		=> 0
        , 'user_defined'	=> 1
        , 'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
    )
);

$this->addAttribute('catalog_product'
    , 'xs_lamp_text'
    , array(
        'group'			    => 'Energy Label'
        , 'input'			=> 'text'
        , 'type'			=> 'varchar'
        , 'label'			=> 'Lamp label text (max length = 100)'
        , 'backend'			=> ''
        , 'frontend'        => ''
        , 'frontend_class'  => 'validate-length maximum-length-100'
        , 'class'           => ''
        , 'default'         => ''
        , 'visible'			=> 1
        , 'filterable'		=> 0
        , 'searchable'		=> 0
        , 'comparable'		=> 0
        , 'visible_on_front'=> 0
        , 'unique'		    => 0
        , 'required'		=> 0
        , 'user_defined'	=> 1
        , 'global'			=> Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
    )
);

$this->addAttribute('catalog_product'
    , 'xs_energy_label'
    , array(
        'type'              => 'varchar'
        , 'label'           => 'Energy Label Image'
        , 'input'           => 'media_image'
        , 'frontend'        => 'catalog/product_attribute_frontend_image'
        , 'required'        => false
        , 'sort_order'      => 4
        , 'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
        , 'group'           => 'Images'
        , 'visible'			=> 1
        , 'filterable'		=> 0
        , 'searchable'		=> 0
        , 'comparable'		=> 0
        , 'visible_on_front'=> 1
        , 'unique'		    => 0
        , 'required'		=> 0
        , 'user_defined'	=> 0
    )
);

$this->addAttribute('catalog_product'
    , 'xs_energy_label_mark'
    , array(
        'type'              => 'varchar'
        , 'label'           => 'Energy Label Mark'
        , 'input'           => 'media_image'
        , 'frontend'        => 'catalog/product_attribute_frontend_image'
        , 'required'        => false
        , 'sort_order'      => 5
        , 'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
        , 'group'           => 'Images'
        , 'visible'			=> 1
        , 'filterable'		=> 0
        , 'searchable'		=> 0
        , 'comparable'		=> 0
        , 'visible_on_front'=> 1
        , 'unique'		    => 0
        , 'required'		=> 0
        , 'user_defined'	=> 0
    )
);

//$this->removeAttribute('catalog_product', 'xs_energy_label');

$this->endSetup();
